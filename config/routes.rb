Rails.application.routes.draw do
  root to: 'home#index'

  devise_for :users, controllers: { omniauth_callbacks: "omniauth_callbacks" }
  # We're only using omniauth for now, so need to add our own signout route
  devise_scope :user do
    delete 'signout(.:format)' => 'devise/sessions#destroy', as: :destroy_user_session
  end

  resources :repos, only: [:index, :show] do
    resources :builds, only: [:index, :show] do
      resources :jobs, controller: :build_jobs, only: :show
    end
  end

  post 'notifications/travis', to: 'notifications#travis'
end
