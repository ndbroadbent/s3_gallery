Paperclip.interpolates :repo do |attachment, style|
  attachment.instance.build.repo.path
end

Paperclip.interpolates :build_number do |attachment, style|
  attachment.instance.build.number
end

Paperclip.interpolates :job_number do |attachment, style|
  attachment.instance.job.number
end
