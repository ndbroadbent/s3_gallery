# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141210204403) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "build_job_artifacts", force: true do |t|
    t.string   "artifact_file_name"
    t.integer  "build_job_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "width",                 default: 0
    t.integer  "height",                default: 0
    t.string   "artifact_content_type"
    t.integer  "artifact_file_size"
    t.datetime "artifact_updated_at"
    t.string   "description"
    t.string   "status"
  end

  add_index "build_job_artifacts", ["build_job_id"], name: "index_build_job_artifacts_on_build_job_id", using: :btree

  create_table "build_jobs", force: true do |t|
    t.integer  "number"
    t.integer  "build_id"
    t.integer  "result"
    t.datetime "started_at"
    t.datetime "finished_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "build_jobs", ["build_id"], name: "index_build_jobs_on_build_id", using: :btree

  create_table "builds", force: true do |t|
    t.integer  "number"
    t.integer  "result"
    t.string   "status_message"
    t.string   "build_url"
    t.string   "build_type"
    t.string   "pull_request_number"
    t.string   "branch"
    t.string   "commit"
    t.string   "author_name"
    t.string   "author_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "compare_url"
    t.integer  "repo_id"
    t.text     "commit_message"
    t.datetime "committed_at"
  end

  add_index "builds", ["repo_id"], name: "index_builds_on_repo_id", using: :btree

  create_table "repos", force: true do |t|
    t.string   "owner"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "app_type"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "provider"
    t.string   "uid"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
