class AddRepoIdToBuild < ActiveRecord::Migration
  def change
    add_reference :builds, :repo, index: true
    remove_column :builds, :repo_name
    remove_column :builds, :repo_url
  end
end
