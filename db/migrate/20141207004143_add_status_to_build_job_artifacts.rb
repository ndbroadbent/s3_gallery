class AddStatusToBuildJobArtifacts < ActiveRecord::Migration
  def change
    add_column :build_job_artifacts, :status, :string
  end
end
