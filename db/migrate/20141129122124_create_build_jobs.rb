class CreateBuildJobs < ActiveRecord::Migration
  def change
    create_table :build_jobs do |t|
      t.integer :number
      t.references :build, index: true
      t.integer :result

      t.datetime :started_at
      t.datetime :finished_at

      t.timestamps
    end
  end
end
