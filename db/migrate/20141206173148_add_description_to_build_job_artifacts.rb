class AddDescriptionToBuildJobArtifacts < ActiveRecord::Migration
  def up
    add_column :build_job_artifacts, :description, :string
    BuildJobArtifact.reset_column_information
    BuildJobArtifact.all.each do |artifact|
      artifact.save!
    end
  end

  def down
    remove_column :build_job_artifacts, :description
  end
end
