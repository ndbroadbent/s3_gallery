class CreateBuilds < ActiveRecord::Migration
  def change
    create_table :builds do |t|
      t.integer :number, index: true
      t.integer :result
      t.string :status_message
      t.string :build_url
      t.string :build_type
      t.string :pull_request_number
      t.string :branch
      t.string :commit
      t.string :committed_at
      t.string :author_name
      t.string :author_email
      t.string :repo_name
      t.string :repo_url

      t.timestamps
    end
  end
end
