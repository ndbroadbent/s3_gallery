class AddPaperclipColumnsToBuildJobArtifacts < ActiveRecord::Migration
  def change
    add_attachment :build_job_artifacts, :artifact

    remove_column :build_job_artifacts, :source, :string
    remove_column :build_job_artifacts, :artifact_file_name, :string

    rename_column :build_job_artifacts, :key, :artifact_file_name

    BuildJobArtifact.update_all artifact_content_type: "image/png", artifact_updated_at: Time.now
  end
end
