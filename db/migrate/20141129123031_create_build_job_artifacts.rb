class CreateBuildJobArtifacts < ActiveRecord::Migration
  def change
    create_table :build_job_artifacts do |t|
      t.string :key
      t.references :build_job, index: true

      t.timestamps
    end
  end
end
