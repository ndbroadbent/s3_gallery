class FixCommittedAtColumn < ActiveRecord::Migration
  def change
    remove_column :builds, :committed_at
    add_column :builds, :committed_at, :datetime
  end
end
