class ChangeBuildCommitMessageToText < ActiveRecord::Migration
  def up
    change_column :builds, :commit_message, :text
  end
end
