class AddSourceToBuildJobArtifacts < ActiveRecord::Migration
  def change
    add_column :build_job_artifacts, :source, :string
  end
end
