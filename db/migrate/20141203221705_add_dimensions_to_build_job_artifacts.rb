class AddDimensionsToBuildJobArtifacts < ActiveRecord::Migration
  def change
    add_column :build_job_artifacts, :width, :integer, default: 0
    add_column :build_job_artifacts, :height, :integer, default: 0

    BuildJobArtifact.reset_column_information
    BuildJobArtifact.all.each do |artifact|
      artifact.set_width_and_height_from_key
      artifact.save!
    end
  end
end
