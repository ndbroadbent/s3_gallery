class AddAppTypeToRepo < ActiveRecord::Migration
  def change
    add_column :repos, :app_type, :string
  end
end
