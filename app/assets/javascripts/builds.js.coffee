# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  setScreenshotAspectRatio = (containers) ->
    containers.each (i, container) ->
      $container = $(container)
      $screenshot = $container.find('img.screenshot')

      aspectRatio = $screenshot.data('aspect-ratio')
      if aspectRatio > 0
        width = $container.width()
        height = width / aspectRatio
        $screenshot.css('width', width)
        $screenshot.css('height', height)

  $('.screenshots.carousel').slick
    lazyLoad: 'ondemand'
    dots: true
    infinite: true
    slidesToShow: 5
    slidesToScroll: 5
    onInit:        -> setScreenshotAspectRatio($('.screenshots.carousel .screenshot-container'))
    onSetPosition: -> setScreenshotAspectRatio($('.screenshots.carousel .screenshot-container'))

  carouselActive = ->
    !!$('.row .header .buttons a.show-carousel.active').length

  # Keep track of lightbox state, foundation's events are broken
  # (change doesn't seem to fire, only open -> opened)
  lightboxOpen = false
  # Opening the lightbox messes up our scroll position,
  # we need to capture and restore it on close
  scrollPosition = $(window).scrollTop()

  # When clicking on a screenshot in the carousel, open up the lightbox
  # by clicking on the hidden grid image for the given index
  $('.screenshots.carousel').on 'click', 'a.screenshot-link', (e) ->
    e.preventDefault()
    index = $(e.currentTarget).data('index')
    container = $(e.currentTarget).closest('.screenshots.carousel')
    nearestLightbox = container.prev().find('.screenshots.lightbox')
    $(nearestLightbox.find('li > a')[index]).click()

  # Hide lightbox when clicking on background
  $('.screenshots.lightbox').on 'click', '.visible-img', (e) ->
    e.preventDefault()
    if $(e.toElement).is('.visible-img')
      $('.screenshots.lightbox a.clearing-close').click()

  # Hide/show .screenshots when thumbnails are opened / closed
  # This is a hack to get around flickering due to translate32 / z-index issues.
  # We only need to do this if the carousel is active
  $('body').on "open.fndtn.clearing",  (e) ->
    scrollPosition = $(window).scrollTop()

    if !lightboxOpen && carouselActive()
      row = $(e.target).closest('.row')
      lightbox = row.find('.screenshots.lightbox')
      # Absolute position so we don't affect other elements
      lightbox.css('position', 'absolute').
               css('visibility', 'hidden').
               show()

  $('body').on "opened.fndtn.clearing", (e) ->
    # Set up previous button next to description
    changedScreenshot = $('li.visible img.screenshot.changed')
    if changedScreenshot.length
      # Clear all existing toggleable images
      $('.visible-img img.previous, .visible-img img.current').remove()

      buttonHTML = "&nbsp;&mdash; <a class='toggle-version' href='#'>Show Previous Version</a>"
      $('.visible-img .clearing-caption').append(buttonHTML)

      $('.visible-img img').addClass('original')

      # Insert previous image next to original
      previousSrc = $('li.visible img.screenshot.changed').data('previous-version-url')
      imageHTML = "<img src=\"#{previousSrc}\" alt=\"Previous Screenshot Version\" class=\"previous hide\"/>"
      $('.visible-img img').after(imageHTML)

      # Copy margins from original image
      $('.visible-img img.previous').css('margin-left', $('.visible-img img.original').css('margin-left'))
      $('.visible-img img.previous').css('margin-top',  $('.visible-img img.original').css('margin-top'))


    if !lightboxOpen
      lightboxOpen = true
      # Hide all other screenshots
      $('.screenshots.carousel, .screenshots.lightbox').hide()
      # Only show our lightbox
      row = $(e.target).closest('.row')
      lightbox = row.find('.screenshots.lightbox')
      lightbox.css('position', 'relative').
               css('visibility', 'visible').
               show()

  $('body').on "click", "a.toggle-version", (e) ->
    e.preventDefault()
    originalIsVisible = $('.visible-img img.original').is(":visible")
    $('.visible-img img.original').toggle(!originalIsVisible)
    $('.visible-img img.previous').toggle(originalIsVisible)

    newLinkText = if originalIsVisible then "Show Current Version" else "Show Previous Version"
    $('a.toggle-version').html(newLinkText)


  $('body').on "close.fndtn.clearing", (e) ->
    showCarousel = carouselActive()
    $('.screenshots.carousel').toggle(showCarousel)
    $('.screenshots.lightbox').toggle(!showCarousel)
    $('.screenshots.carousel').slickSetOption(null, null, true)

  $('body').on "closed.fndtn.clearing", (e) ->
    lightboxOpen = false
    $(window).scrollTop(scrollPosition)

  $('.row .header').on 'click', '.buttons a', (e) ->
    e.preventDefault()

    button = $(e.currentTarget)
    return if button.is('.active')

    $('.row .header .buttons a').removeClass('active')
    button.addClass('active')

    showCarousel = button.is('a.show-carousel')
    $('.screenshots.carousel').toggle(showCarousel)
    $('.screenshots.lightbox').toggle(!showCarousel)

    if showCarousel
      $.cookie("preferred-layout", 'carousel', path: '/')
      # Forces a UI refresh
      $('.screenshots.carousel').slickSetOption(null, null, true)

    else
      $.cookie("preferred-layout", 'grid', path: '/')
      # Now that grid is shown, calculate correct height for placeholders
      setScreenshotAspectRatio($('.screenshots.lightbox li a'))


  # Now fetch preference from cookie and switch to preferred layout
  if $.cookie("preferred-layout") == 'grid'
    $('a.show-grid').click()

