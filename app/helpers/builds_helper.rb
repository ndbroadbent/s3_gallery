module BuildsHelper
  def latest_build
    @latest_build ||= Build.order('created_at DESC').first
  end

  def class_for_build_status(build)
    if build.passed?
      'passed'
    else
      if build.status_message == 'Errored'
        'errored'
      else
        'failed'
      end
    end
  end

  def class_for_build_job_status(build_job)
    build_job.passed? ? 'passed' : 'failed'
  end

  def class_for_artifact_status(artifact)
    case artifact.status
    when 'changed'
      'changed'
    when 'new'
      'new'
    else
      ''
    end
  end
end
