class Repo < ActiveRecord::Base
  has_many :builds
  validates :app_type, inclusion: { in: %w(iphone ipad ios-universal web) }, allow_blank: true

  def to_param
    name.to_s
  end

  def path
    [owner, name].join('/')
  end

  def github_url
    "https://github.com/#{path}"
  end
end
