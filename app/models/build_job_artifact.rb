class BuildJobArtifact < ActiveRecord::Base
  belongs_to :job, class_name: "BuildJob", foreign_key: :build_job_id, inverse_of: :artifacts
  delegate :build, to: :job
  delegate :repo, to: :build

  before_save :set_description_from_filename
  before_save :set_width_and_height_from_filename

  scope :all_changes, -> { where(status: [:changed, :new]) }
  scope :changed_images, -> { where(status: :changed) }
  scope :new_images, -> { where(status: :new) }

  def changed?
    status.to_s.in?(['changed', 'new'])
  end

  paperclip_opts = {
    url:  '/screenshots/:build_number/:job_number/:filename',
    path: ':rails_root/public/screenshots/:build_number/:job_number/:filename',
    convert_options: { all: '-quality 95' },
    restricted_characters: /@/
  }
  if Rails.env.development? || Rails.env.production?
    paperclip_opts.merge!(
      storage: :s3,
      s3_permissions: :private,
      url:  '/:repo/:build_number/:job_number/:filename',
      path: '/:repo/:build_number/:job_number/:filename'
    )
  end

  has_attached_file :artifact, paperclip_opts

  validates_attachment_content_type :artifact, :content_type => ["image/jpeg", "image/gif", "image/png"]

  def previous_artifact_version
    return nil unless changed?

    BuildJobArtifact.joins(job: {build: :repo}).where(
      'repos.id' => repo.id,
      'build_job_artifacts.artifact_file_name' => artifact_file_name,
      'builds.branch' => 'master'
    ).where(
      'builds.id < ?', self.job.build_id
    ).order('build_job_artifacts.id DESC').
    first
  end

  def previous_artifact_version_url(expires = 15.minutes)
    previous_artifact = previous_artifact_version
    if previous_artifact
      previous_artifact.artifact.expiring_url(expires)
    end
  end

  def update_status!
    # Check for duplicates
    self.status = :new

    previous_artifact = self.previous_artifact_version

    if previous_artifact
      begin
        current = Tempfile.new('current_artifact')
        current.binmode
        current.write(self.artifact.s3_object.read)
        current.rewind

        previous = Tempfile.new('previous_artifact')
        previous.binmode
        previous.write(previous_artifact.artifact.s3_object.read)
        previous.rewind

        current_image  = Phashion::Image.new(current.path)
        previous_image = Phashion::Image.new(previous.path)

        # Images should be nearly identical. We want to know if tiny things change.
        if current_image.duplicate?(previous_image, threshold: 0)
          self.status = :unchanged
        else
          self.status = :changed
        end

      ensure
        current.close
        previous.close
      end
    end

    save!
  end


  def aspect_ratio
    return nil if width == 0 || height == 0
    (width.to_f / height.to_f).round(5)
  end

  # Extract description from key without extension.
  # Also remove any dimensions
  # Examples:
  #     "15.1/KIF+SwiftExensions.swift, line 11.png"
  #  => "KIF+SwiftExensions.swift, line 11"
  #
  #     "15/1/AA01 Add Post-640x960.png"
  #  => "Add Post"
  def set_description_from_filename
    self.description = artifact_file_name.
      to_s[/([^\/]+)\.png/, 1].to_s.
      sub(/-\d+x\d+/, '').       # Strip dimensions
      sub(/^[A-Za-z]+\d+ /, '')  # Strip ordering prefix
  end

  def set_width_and_height_from_filename
    if dimensions = artifact_file_name.to_s[/-(\d+x\d+)\./, 1]
      self.width, self.height = dimensions.split('x')
    end
  end
end
