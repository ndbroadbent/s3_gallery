class BuildJob < ActiveRecord::Base
  belongs_to :build, inverse_of: :jobs
  has_many :artifacts, class_name: "BuildJobArtifact", inverse_of: :job, dependent: :destroy

  def to_param
    number.to_s
  end

  def passed?
    result == 0
  end

  def travis_build_number
    [build.number, self.number].join('.')
  end

  def job_for_previous_build
    Build.where('id < ?', build_id).order('id DESC').first.jobs.where(number: number).first
  end

  def fetch_build_job_artifacts_from_s3
    # Clear any existing artifacts
    artifacts.delete_all

    prefix = [build.repo.path, build.number, self.number, ""].join("/")
    S3BucketService.list_objects_for_prefix(prefix) do |object|
      next if object.key.ends_with?('/')  # Ignore "folders"
      next if object.key =~ /\/medium\//

      filename = object.key.to_s[/([^\/]+\.[a-z]+)/, 1].to_s
      extension = filename[/\.([a-z]+)$/, 1]

      build_job_artifact = artifacts.where(artifact_file_name: filename).first_or_create(
        artifact_content_type: "image/#{extension}"
      )

      build_job_artifact.update_status!
    end
  end
end
