class Build < ActiveRecord::Base
  belongs_to :repo
  has_many :jobs, class_name: "BuildJob", inverse_of: :build, dependent: :destroy
  scope :with_artifacts, -> { includes(jobs: :artifacts).where.not(build_job_artifacts: { id: nil }) }

  def artifacts
    if new_record?
      BuildJobArtifact.none
    else
      BuildJobArtifact.joins(job: :build).where('builds.id' => id)
    end
  end

  scope :failed, -> { where(result: 1) }

  def to_param
    number.to_s
  end

  def short_commit
    commit.to_s[0...7]
  end

  def passed?
    result == 0
  end
end
