class S3BucketService
  class << self
    def bucket
      @bucket ||= AWS::S3.new.buckets[ENV['S3_BUCKET_NAME']]
    end

    def list_objects_for_prefix(prefix, each_args = {})
      Rails.logger.info "[S3BucketService] Fetching list of objects for #{prefix} ..."
      bucket.objects.with_prefix(prefix).each(each_args) do |object|
        yield object
      end
    end

    def object_for_key(key)
      bucket.objects[key]
    end
  end
end
