module Notifiers
  class GithubNotifierService < Base
    def post_build_notification
      # For now, only comment on pull requests
      return unless build.pull_request_number?

      # Only post if the build has any artifacts
      return unless build.artifacts.count > 0

      github = Github.new(oauth_token: ENV['GITHUB_API_ACCESS_TOKEN'])

      github.issues.comments.create(
        user:   build.repo.owner,
        repo:   build.repo.name,
        number: build.pull_request_number,
        body:   message
      )
    end


    private

    # Markdown links
    def link_to(name, url)
      "[#{name}](#{url})"
    end
  end
end
