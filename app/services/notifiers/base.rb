module Notifiers
  class Base
    include Rails.application.routes.url_helpers
    include ActionView::Helpers::TextHelper

    attr_accessor :build

    def initialize(build)
      @build = build
    end

    def message
      <<-EOF
#{link_to 'Screenshot gallery is ready', gallery_url} \
for #{link_to build.repo.path, repo_url(build.repo)} \
build #{link_to "##{build.number}", build.build_url} \
(#{link_to build.short_commit, build.compare_url} - #{build.branch}) \
(#{screenshots_message})
EOF
    end


    private

    def screenshots_message
      links = []

      links << link_to(pluralize(build.artifacts.count, 'screenshot'), gallery_url)

      changed_count = build.artifacts.changed_images.count
      if changed_count > 0
        links << link_to("#{changed_count} changed", gallery_url(changes: 1))
      end

      new_count = build.artifacts.new_images.count
      if new_count > 0
        links << link_to("#{new_count} new", gallery_url(changes: 1))
      end

      links.join(', ')
    end

    def link_to(name, url)
      raise "Please implement #link_to in your subclass"
    end

    def gallery_url(options = {})
      repo_build_url(build.repo, build, options)
    end
  end
end
