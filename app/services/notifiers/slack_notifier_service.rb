module Notifiers
  class SlackNotifierService < Base
    def post_build_notification
      # Only post if the build has any artifacts
      return unless build.artifacts.count > 0

      notifier = Slack::Notifier.new ENV['SLACK_WEBHOOK_URL']

      icon_url = [ENV["DOMAIN_NAME"], 'favicon-48.png'].join('/')
      notifier.ping message, icon_url: icon_url
    end


    private

    # Weird Slack link syntax
    def link_to(name, url)
      "<#{url}|#{name}>"
    end
  end
end
