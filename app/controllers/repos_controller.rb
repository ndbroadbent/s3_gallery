class ReposController < ApplicationController
  before_filter :authenticate_user!

  def index
    @repos = Repo.order('created_at DESC').page(params[:page])

    if @repos.count == 1
      redirect_to repo_builds_path(@repos.first)
    end
  end

  def show
    repo = Repo.find_by_name!(params[:id])
    redirect_to repo_builds_path(repo)
  end
end
