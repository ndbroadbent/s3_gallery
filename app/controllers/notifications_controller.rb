class NotificationsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  #before_filter :authenticate_travis_request!, only: [:travis]

  def travis
    payload = JSON.parse(params[:payload])

    repo_data = payload["repository"]
    repo = Repo.where(owner: repo_data["owner_name"], name: repo_data["name"]).first_or_create

    # Check for duplicate POSTs
    if repo.builds.where(number: payload["number"]).exists?
      head :ok and return
    end

    build = repo.builds.build(
      number:              payload["number"],
      result:              payload["result"],
      status_message:      payload["status_message"],
      build_url:           payload["build_url"],
      build_type:          payload["type"],
      pull_request_number: payload["pull_request_number"],
      branch:              payload["branch"],
      commit:              payload["commit"],
      commit_message:      payload["message"],
      committed_at:        payload["committed_at"],
      author_name:         payload["author_name"],
      author_email:        payload["author_email"],
      compare_url:         payload["compare_url"]
    )

    if !build.save
      head :unprocessable_entity and return
    end

    payload["matrix"].each do |job|
      # Remove master build number from job number
      # "15.2" => "2"
      job_number = job["number"].to_s.sub(/^\d+\./, '')

      job = build.jobs.create(
        number:      job_number,
        result:      job["result"],
        started_at:  job["started_at"],
        finished_at: job["finished_at"]
      )

      job.fetch_build_job_artifacts_from_s3
    end

    Notifiers::SlackNotifierService.new(build).post_build_notification
    Notifiers::GithubNotifierService.new(build).post_build_notification

    head :created
  end


  protected

  def authenticate_travis_request!
    expected_digest = generate_request_digest(request.env['HTTP_TRAVIS_REPO_SLUG'], ENV['TRAVIS_USER_TOKEN'])
    unless expected_digest == request.env['HTTP_AUTHORIZATION']
      head :unauthorized
    end
  end

  def generate_request_digest(*args)
    Digest::SHA2.new.update(args.join).to_s
  end
end
