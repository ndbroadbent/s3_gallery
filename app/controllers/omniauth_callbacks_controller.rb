class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  before_filter :authenticate_github_user!

  def github
    @user = User.from_omniauth(request.env["omniauth.auth"])
    sign_in_and_redirect @user, :event => :authentication #this will throw if @user is not activated
    set_flash_message(:notice, :success, :kind => "GitHub") if is_navigational_format?
  end


  protected

  def authenticate_github_user!
    return unless ENV['GITHUB_ORGANIZATION']

    # Check if github username is whitelisted
    github_username = request.env["omniauth.auth"].try(:info).try(:nickname)
    if github_username.present? && ENV['GITHUB_USERNAMES'].present?
      if ENV['GITHUB_USERNAMES'].split(',').include?(github_username)
        return true
      end
    end

    # If username check fails, use the OAuth token to look up and compare the user's organizations
    oauth_token = request.env["omniauth.auth"].try(:credentials).try(:token)
    unless oauth_token.present?
      flash[:error] = "Sorry, something went wrong! #{params[:error_description]}"
      redirect_to root_path and return
    end

    github = Github.new(oauth_token: oauth_token)

    organizations = github.organizations.list.map(&:login)
    unless organizations.include?(ENV['GITHUB_ORGANIZATION'])
      flash[:error] = "Sorry, you don't belong to our GitHub organization!"
      redirect_to root_path
    end
  end
end
