class BuildsController < ApplicationController
  PER_PAGE = 9

  before_filter :authenticate_user!


  def index
    @repo = Repo.find_by_name!(params[:repo_id])
    @builds = @repo.builds.with_artifacts

    if params[:filter] == 'failed'
      @builds = @builds.failed
    end

    @builds = @builds.
      order('builds.created_at DESC').
      page(params[:page]).
      per(PER_PAGE)

    # Check to see if any of the builds have multiple jobs,
    # otherwise we can hide the jobs column.
    @has_multiple_jobs = @builds.count('build_jobs.id') > @builds.count
  end

  def show
    @build = Build.joins(:repo).where(
      'repos.name' => params[:repo_id],
      'builds.number' => params[:id]
    ).first

    unless @build
      flash[:error] = "Build not found!"
      redirect_to repo_path(params[:repo_id])
    end
  end
end
