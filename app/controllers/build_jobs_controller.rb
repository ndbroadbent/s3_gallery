class BuildJobsController < ApplicationController
  before_filter :authenticate_user!

  def show
    @build_job = BuildJob.joins(build: :repo).where(
      'repos.name' => params[:repo_id],
      'builds.number' => params[:build_id],
      'build_jobs.number' => params[:id]
    ).first

    unless @build_job
      flash[:error] = "Job not found!"
      redirect_to repo_build_path(params[:repo_id], params[:build_id])
    end
  end
end
