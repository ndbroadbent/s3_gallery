describe Notifiers::Base do
  class TestNotifier < Notifiers::Base
    def link_to(name, url)
      "#{name}::#{url}"
    end
  end

  let(:build) { create :build, pull_request_number: 1337, branch: "test_branch" }
  let(:build_job) { create :build_job, build: build }
  let!(:build_job_artifact) { create :build_job_artifact, job: build_job, status: :unchanged }

  subject do
    TestNotifier.new(build)
  end

  describe "#message" do
    it "should generate the correct notification message" do
      message = subject.message

      expect(message).to include "Screenshot gallery is ready"
      expect(message).to include "- test_branch"
      expect(message).to include build.commit
      expect(message).to include "1 screenshot::"
      expect(message).to_not include "changed::"
      expect(message).to_not include "new::"
    end
  end

  context "with changed and new screenshots" do
    let!(:changed_artifact) { create :build_job_artifact, job: build_job, status: :changed }
    let!(:new_artifact) { create :build_job_artifact, job: build_job, status: :new }

    it "should generate the correct notification message" do
      message = subject.message

      expect(message).to include "3 screenshots::"
      expect(message).to include "1 changed::"
      expect(message).to include "1 new::"
    end
  end
end
