describe Notifiers::SlackNotifierService do
  let(:slack_webhook_url) { 'http://slack.example.com/123456789abcdef' }

  let(:build) { create :build, pull_request_number: 1337, branch: "test_branch" }
  let(:build_job) { create :build_job, build: build }
  let!(:build_job_artifact) { create :build_job_artifact, job: build_job, status: :unchanged }

  subject do
    Notifiers::SlackNotifierService.new(build).post_build_notification
  end

  before do
    # Stub github organization for tests
    allow(ENV).to receive(:[]).and_call_original
    allow(ENV).to receive(:[]).with('SLACK_WEBHOOK_URL') { slack_webhook_url }

    @slack_mock = double
    expect(Slack::Notifier).to receive(:new).with(slack_webhook_url) { @slack_mock }
  end

  it "should call the Slack notification API" do
    expect(@slack_mock).to receive(:ping) do |message, params|
      expect(message).to include "Screenshot gallery is ready"
      expect(params[:icon_url]).to include "favicon-48"
    end

    subject
  end
end
