describe Notifiers::GithubNotifierService do
  let(:github_token) { 'abcdef1234567890' }

  let(:build) { create :build, pull_request_number: 1337, branch: "test_branch" }
  let(:build_job) { create :build_job, build: build }
  let!(:build_job_artifact) { create :build_job_artifact, job: build_job, status: :unchanged }

  subject do
    Notifiers::GithubNotifierService.new(build).post_build_notification
  end

  before do
    # Stub github organization for tests
    allow(ENV).to receive(:[]).and_call_original
    allow(ENV).to receive(:[]).with('GITHUB_API_ACCESS_TOKEN') { github_token }

    github_mock = double
    expect(Github).to receive(:new).with(oauth_token: github_token) { github_mock }

    @comments_api = double
    allow(github_mock).to receive_message_chain(:issues, :comments) { @comments_api }
  end

  it "should call the GitHub comments API" do
    expect(@comments_api).to receive(:create) do |params|
      expect(params[:user]).to eq build.repo.owner
      expect(params[:repo]).to eq build.repo.name
      expect(params[:number]).to eq build.pull_request_number
      expect(params[:body]).to include "Screenshot gallery is ready"
    end

    subject
  end
end
