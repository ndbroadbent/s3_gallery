include ActionDispatch::TestProcess

FactoryGirl.define do
  factory :build_job_artifact do
    association :job, factory: :build_job
    artifact do
      screenshot_image = Rails.root.join('spec/fixtures/test_screenshot.png')
      fixture_file_upload(screenshot_image, 'image/png')
    end
  end
end
