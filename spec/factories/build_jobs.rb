FactoryGirl.define do
  factory :build_job do
    build
    number { 1 }
    result 1
    started_at { 10.minutes.ago }
    finished_at { 5.minutes.ago }
  end
end
