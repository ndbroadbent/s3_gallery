FactoryGirl.define do
  factory :build do
    repo
    number { 1 }
    result 1
    status_message { 'Failed' }
    commit { SecureRandom.hex(4).first(7) }
    branch { 'master' }
    build_url { "http://ci.example.com/builds/#{number}" }
    compare_url { "http://git.example.com/commits/#{commit}" }
    author_name { "Test Committer" }
    author_email { "tcommitter@example.com" }
    commit_message { "Test Commit" }
  end
end
