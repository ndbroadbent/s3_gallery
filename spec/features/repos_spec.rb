require 'rails_helper'

describe "Repos", type: :feature, js: true do
  require_signed_in_user!

  before do
    create :repo, name: 'testrepo-ios', owner: 'testorg', app_type: 'iphone'
    create :repo, name: 'testrepo-web', owner: 'testorg', app_type: 'web'
  end

  it "shows the repos index page" do
    visit root_path
    expect(page).to have_content "testorg/testrepo"
    save_screenshot('Repos Index')
  end
end
