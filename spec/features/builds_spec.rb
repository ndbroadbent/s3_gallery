require 'rails_helper'

describe "Builds", type: :feature, js: true do
  require_signed_in_user!

  let!(:repo) { create :repo, name: 'testrepo', owner: 'testorg' }

  before do
    # Create one build without artifacts
    create :build, repo: repo, number: 1, result: 1, commit: "abcdef3"

    # Create 2 builds with artifacts
    2.times do |i|
      build = create(:build,
        repo: repo,
        number: i + 2,
        result: 1,
        commit: "abcdef#{i + 1}",
        commit_message: "Test Commit",
        author_name: "Test Author",
        author_email: "t.author@example.com",
      )
      create :build_job, build: build
    end

    artifact = create :build_job_artifact,
      job: BuildJob.first,
      status: :new,
      artifact: fixture_file_upload('test_screenshot.png', 'image/png')

    # Overwrite screenshot with previous version
    previous_screenshot = Rails.root.join('spec/fixtures/test_screenshot_previous.png')
    system "cp #{previous_screenshot} #{artifact.artifact.path}"

    artifact = create :build_job_artifact,
      job: BuildJob.last,
      status: :unchanged,
      artifact: fixture_file_upload('test_screenshot.png', 'image/png')

    artifact = create :build_job_artifact,
      job: BuildJob.last,
      status: :changed,
      artifact: fixture_file_upload('test_screenshot.png', 'image/png')

    artifact = create :build_job_artifact,
      job: BuildJob.last,
      status: :new,
      artifact: fixture_file_upload('test_screenshot.png', 'image/png')
  end

  describe "builds index" do
    it "shows the builds index page" do
      visit repo_builds_path(repo)

      # Ensure that builds are listed
      expect(page).to have_content "abcdef1"
      expect(page).to have_content "abcdef2"

      # Ensure that we don't display builds without any artifacts
      expect(page).to_not have_content "abcdef3"

      save_screenshot('Builds Index')
    end
  end

  describe "builds show" do
    let(:build) { Build.last }

    it "shows the build screenshots gallery" do
      visit repo_build_path(repo, build)

      expect(page).to have_content "abcdef2"
      expect(page).to have_content "Test Commit"

      expected_url = build.artifacts.first.artifact.url
      expect(page).to have_xpath("//img[@src='#{expected_url}']")

      save_screenshot('Builds Gallery')

      all("img.screenshot.changed").first.click
      expect(page).to have_selector('.visible-img img', visible: true)
      save_screenshot('Builds Gallery - Fullscreen')

      click_link "Show Previous Version"
      expect(page).to have_selector('.visible-img img', visible: true)

      save_screenshot('Builds Gallery - Fullscreen - Show Previous')
    end
  end
end
