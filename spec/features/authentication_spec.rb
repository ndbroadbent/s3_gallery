require 'rails_helper'

describe "Authentication", type: :feature, js: true do
  let(:github_uid) { '1234567' }
  let(:github_token) { 'abcdef1234567890' }
  let(:email) { 'githubuser@example.com' }
  let(:github_username) { 'example_user' }
  let(:github_organizations) { ['hdwr', 'rails', 'linux'] }

  def self.mock_github_organizations!
    before do
      # Stub GitHub organizations lookup via API
      github_mock = double
      allow(github_mock).to receive_message_chain(:organizations, :list, :map) { github_organizations }
      expect(Github).to receive(:new).with(oauth_token: github_token) { github_mock }
    end
  end

  before do
    # Create a couple of repos so that authenticated screenshot isn't blank
    create :repo, name: 'testrepo-ios', owner: 'testorg', app_type: 'iphone'
    create :repo, name: 'testrepo-web', owner: 'testorg', app_type: 'web'

    OmniAuth.config.test_mode = true
    OmniAuth.config.add_mock(:github, {
      uid: github_uid,
      credentials: {
        token: github_token
      },
      info: {
        email: email,
        nickname: github_username
      }
    })

    # Stub github organization for tests
    allow(ENV).to receive(:[]).and_call_original
    allow(ENV).to receive(:[]).with('GITHUB_ORGANIZATION') { 'testorganization' }
  end

  context "when signing up as a new user" do
    context "and user belongs to correct GitHub organization" do
      mock_github_organizations!
      let(:github_organizations) { ['hdwr', 'rails', 'linux', 'testorganization'] }

      it "creates a new user and signs in" do
        visit '/'
        save_screenshot('Unauthenticated')

        click_link 'Sign in with GitHub'

        expect(page).to have_content 'Successfully authenticated from GitHub account'
        save_screenshot('Authenticated')

        expect(User.count).to eq 1
        user = User.first
        expect(user.provider).to eq 'github'
        expect(user.email).to eq email
      end
    end

    context "and user does not belong to the correct GitHub organization" do
      mock_github_organizations!

      it "does not create a new user or sign in" do
        visit '/'
        click_link 'Sign in with GitHub'

        expect(page).to have_content "Sorry, you don't belong to our GitHub organization!"
        expect(User.count).to eq 0

        save_screenshot('Authentication Failed')
      end
    end

    context "and GitHub username is specifically whitelisted" do
      before do
        allow(ENV).to receive(:[]).with('GITHUB_USERNAMES') { "ausername,#{github_username},anotherusername" }
      end

      it "creates a new user and signs in" do
        visit '/'
        click_link 'Sign in with GitHub'

        expect(page).to have_content 'Successfully authenticated from GitHub account'
      end
    end
  end

  context "when signing in as an existing user" do
    mock_github_organizations!

    let!(:user) { create(:user, provider: 'github', uid: github_uid) }

    context "and user belongs to the correct GitHub organization" do
      let(:github_organizations) { ['hdwr', 'rails', 'linux', 'testorganization'] }

      it "signs in the user" do
        visit '/'
        click_link 'Sign in with GitHub'

        expect(page).to have_content 'Successfully authenticated from GitHub account'
        expect(User.count).to eq 1
      end
    end

    context "and user no longer belongs to the correct GitHub organization" do
      it "does not sign in the user" do
        visit '/'
        click_link 'Sign in with GitHub'

        expect(page).to have_content "Sorry, you don't belong to our GitHub organization!"
        expect(User.count).to eq 1
      end
    end
  end
end
