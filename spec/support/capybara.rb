Capybara.asset_host = 'http://localhost:3000'

module CapybaraScreenshots
  def save_screenshot(name)
    page.save_screenshot File.join('spec/screenshots', "#{name}.png"), full: true
  end
end
