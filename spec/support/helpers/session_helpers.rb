module Features
  module SessionHelpers
    def require_signed_in_user!
      let(:user) { create :user }
      before do
        login_as(user, scope: :user)
      end
    end
  end
end
