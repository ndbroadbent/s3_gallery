module ScreenshotAssets
  class Engine < Rails::Engine
    initializer 'static_assets.load_static_assets' do |app|
      app.middleware.use ActionDispatch::Static, "#{root}/spec/screenshots"
    end
  end
end
