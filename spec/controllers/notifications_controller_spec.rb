require 'rails_helper'

describe NotificationsController do
  describe "POST travis" do
    let(:payload_json) { File.read(Rails.root.join('spec/fixtures/travis_notification.json')) }
    let(:repo_slug) { "test/example" }
    let(:s3_object_key) { '1/1/Test Screenshot.swift, line 23.png' }

    subject do
      digest = controller.send(:generate_request_digest, repo_slug, ENV['TRAVIS_USER_TOKEN'])
      request.env['HTTP_TRAVIS_REPO_SLUG'] = repo_slug
      request.env['HTTP_AUTHORIZATION'] = digest

      post :travis, payload: payload_json
    end

    it "should accept a valid POST" do
      s3object = double()
      expect(s3object).to receive(:key).exactly(3).times.and_return(s3_object_key)
      expect(S3BucketService).to receive(:list_objects_for_prefix).with('test/example/1/1/').and_yield(s3object)
      expect_any_instance_of(Notifiers::SlackNotifierService).to receive(:post_build_notification).with(any_args)
      expect_any_instance_of(Notifiers::GithubNotifierService).to receive(:post_build_notification).with(any_args)

      expect { subject }.to change(Build, :count).by(1)

      expect(response).to have_http_status(:created)

      build = Build.last
      expect(build.number).to eq 1
      expect(build.status_message).to eq "Passed"
      expect(build.commit).to eq "62aae5f70ceee39123ef"

      expect(build.jobs.count).to eq 1
      job = build.jobs.last
      expect(job.number).to eq 1

      expect(job.artifacts.count).to eq 1
      artifact = job.artifacts.last

      expect(s3_object_key).to include artifact.artifact_file_name
    end
  end
end
