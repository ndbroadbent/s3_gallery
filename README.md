S3 Gallery
================

[![Build Status](https://travis-ci.org/hdwr/s3_gallery.svg?branch=master)](https://travis-ci.org/hdwr/s3_gallery)
[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

I built this as a simple way to view a gallery of CI screenshots.

If you run your iOS tests on Travis CI, follow these instructions to start
taking screenshots of your failed tests: http://andreas.boehrnsen.de/blog/2014/02/dont-be-blind-when-kif-fails-on-travis/


Configuration
-------------

This project uses [dotenv](https://github.com/bkeepers/dotenv), so Rails will load all your configuration as environment variables from a `.env` file.

This makes it very easy to sync config with Heroku: `heroku config:push` and `heroku config:pull`.
You'll need to install the heroku-config plugin for these commands:

```bash
heroku plugins:install git://github.com/ddollar/heroku-config.git
```

Run `cp .env.example .env`, and then fill in the following variables:

* `DOMAIN_NAME` - Where you're host the application.
* `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` - Get these from your AWS Console
* `AWS_REGION` - e.g. "us-west-2"
* `GITHUB_CLIENT_ID` and `GITHUB_CLIENT_SECRET` - Create a new GitHub application
* `GITHUB_ORGANIZATION` - Your company's GitHub organization. Leave blank to let anyone in.
* `S3_BUCKET_NAME` - Name of the bucket where you're storing your screenshots. e.g. `travis.example.com`
* `TRAVIS_USER_TOKEN` - Used to authenticate the webhook notifications from Travis CI
* `SLACK_WEBHOOK_URL` - After the build and screenshots have been registered, post a notification to a slack channel.


Authentication
--------------

Users can authenticate with their GitHub accounts. If you set `GITHUB_ORGANIZATION`, the user will not be allowed to sign in unless they belong to that GitHub organization.


Ruby on Rails
-------------

This application requires:

- Ruby 2.1.3
- Rails 4.1.8

Learn more about [Installing Rails](http://railsapps.github.io/installing-rails.html).


Contributing
------------

Please feel free to make this better! Right now the app only really handles **iOS apps** on **GitHub**, running tests on **Travis CI**, with screenshots uploaded to an **S3 bucket**. Any one of those categories could be expanded, e.g. web apps, Circle CI, HipChat, GitLab, etc. etc.

The README could also use some more detailed instructions.


Credits
-------

Nathan Broadbent (nathan@hdwr.co)

License
-------

MIT